package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 *
 */
@Data
public class DriveInfoResponse {
    private String avatar;

    private String email;

    private String phone;

    private String role;

    private String status;

    private String description;

    private String punishFlagEnum;

    @JSONField(name = "user_id")
    private String userId;

    @JSONField(name = "domain_id")
    private String domainId;

    @JSONField(name = "user_name")
    private String userName;

    @JSONField(name = "nick_name")
    private String nickName;

    @JSONField(name = "default_drive_id")
    private String defaultDriveId;

    @JSONField(name = "created_at")
    private Long createdAt;

    @JSONField(name = "updated_at")
    private Long updatedAt;

    @JSONField(name = "punish_flag")
    private String punishFlag;


}
package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 文件创建
 */
@Data
public class CreateResponse {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 必填
     */
    private String status;

    /**
     * 必填
     */
    @JSONField(name = "parent_file_id")
    private String parentFileId;

    /**
     * 选填	创建文件夹返回空
     */
    @JSONField(name = "upload_id")
    private String uploadId;

    /**
     * 必填
     */
    @JSONField(name = "file_name")
    private String fileName;

    /**
     * boolean
     * 必填
     */
    private boolean available;

    /**
     * 必填
     * 是否存在同名文件
     */
    private boolean exist;

    /**
     * 必填
     * 是否秒传
     */
    @JSONField(name = "rapid_upload")
    private boolean rapidUpload;

    /**
     * 必填
     * file_list
     */
    @JSONField(name = "part_info_list")
    private List<PartInfo> partInfoList;

}

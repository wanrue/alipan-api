package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 授权 code 获取 access_token
 */
@Data
public class TokenResponse {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "token_type")
    private String tokenType;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "access_token")
    private String accessToken;
    /**
     * 必填
     * 父目录id
     */
    @JSONField(name = "refresh_token")
    private String refreshToken;

    /**
     * 必填
     */
    @JSONField(name = "expires_in")
    private Integer expiresIn;

}
package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 刷新获取上传地址
 */
@Data
public class RefreshResponse {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 上传ID
     */
    @JSONField(name = "upload_id")
    private String uploadId;

    /**
     * 格式："yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"	2023-02-14T05:48:46.045Z
     */
    @JSONField(name = "created_at")
    private String createdAt;

    /**
     * 分片信息列表
     */
    @JSONField(name = "part_info_list")
    private List<PartInfo> partInfoList;

}
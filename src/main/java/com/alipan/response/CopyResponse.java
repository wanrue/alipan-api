package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 复制文件或文件夹
 */
@Data
public class CopyResponse {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 异步任务id，有的话表示需要经过异步处理。
     */
    @JSONField(name = "async_task_id")
    private String asyncTaskId;

}
package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 标记文件上传完毕
 */
@Data
public class CompleteResponse {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;
    /**
     * 必填
     * 父目录id
     */
    @JSONField(name = "parent_file_id")
    private String parentFileId;
    /**
     * 必填
     * 文件名
     */
    private String name;
    /**
     * int	必填
     */
    private Long size;
    /**
     * 必填
     */
    @JSONField(name = "file_extension")
    private String fileExtension;
    /**
     * 必填
     * 文件hash
     */
    @JSONField(name = "content_hash")
    private String contentHash;
    /**
     * 必填
     */
    private String category;
    /**
     * 必填
     * file | folderq
     */
    private String type;
    /**
     * 选填
     * 缩略图
     */
    private String thumbnail;
    /**
     * 选填
     * 预览
     */
    private String url;

    /**
     * 选填
     * 下载地址
     */
    @JSONField(name = "download_url")
    private String downloadUrl;

    /**
     * 必填
     * 格式："yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
     */
    @JSONField(name = "created_at")
    private String createdAt;
    /**
     * 必填
     * 格式："yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
     */
    @JSONField(name = "updated_at")
    private String updatedAt;

    /**
     * 文件创建获取的upload_id
     */
    @JSONField(name = "upload_id")
    private String uploadId;

}
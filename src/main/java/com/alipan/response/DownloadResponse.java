package com.alipan.response;

import lombok.Data;

/**
 * 获取文件下载链接
 */
@Data
public class DownloadResponse {

    /**
     * 必填
     * 下载地址
     */
    private String url;
    /**
     * 必填
     * 过期时间 格式："yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
     */
    private String expiration;

    /**
     * 必填
     * 下载方法
     */
    private String method;
}
package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

@Data
public class PartInfo {
    /**
     * 分片编号
     */
    @JSONField(name = "part_number")
    private Integer partNumber;
    /**
     * 分片大小
     */
    @JSONField(name = "part_size")
    private Integer partSize;
    /**
     * 上传地址
     */
    @JSONField(name = "upload_url")
    private String uploadUrl;
}
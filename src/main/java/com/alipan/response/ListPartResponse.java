package com.alipan.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 列举已上传分片
 */
@Data
public class ListPartResponse {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 是否并行上传
     */
    private boolean parallelUpload;

    /**
     * 下一页起始资源标识符, 最后一页该值为空。
     */
    @JSONField(name = "next_part_number_marker")
    private String nextPartNumberMarker;

    /**
     * 已经上传分片列表
     */
    @JSONField(name = "uploaded_parts")
    private List<PartInfo> uploaded_parts;

    @Data
    public static class PartInfo {
        /**
         * etag
         * 在上传分片结束后，服务端会返回这个分片的Etag，在complete的时候可以在uploadInfo指定分片的Etag，服务端会在合并时对每个分片Etag做校验
         */
        private String etag;
        /**
         * 分片编号
         */
        @JSONField(name = "part_number")
        private Integer partNumber;
        /**
         * 分片大小
         */
        @JSONField(name = "part_size")
        private Integer partSize;
    }

}
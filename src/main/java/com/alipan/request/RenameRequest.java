package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 重命名单个文件，收藏单个文件
 */
@Data
@Builder
public class RenameRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 新的文件名
     */
    private String name;

    /**
     * 收藏 true，移除收藏 false
     */
    private boolean starred;

}
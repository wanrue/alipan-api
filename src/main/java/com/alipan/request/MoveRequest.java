package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 移动文件或文件夹
 */
@Data
@Builder
public class MoveRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 必填
     * 父文件ID、根目录为 root
     */
    @JSONField(name = "to_parent_file_id")
    private String toParentFileId;

    /**
     * 同名文件处理模式，可选值如下：
     * ignore：允许同名文件；
     * auto_rename：当发现同名文件是，云端自动重命名。
     * refuse：当云端存在同名文件时，拒绝创建新文件。
     * 默认为 refuse
     */
    @JSONField(name = "check_name_mode")
    private String checkNameMode;

    /**
     * 当云端存在同名文件时，使用的新名字
     */
    @JSONField(name = "new_name")
    private String newName;
}
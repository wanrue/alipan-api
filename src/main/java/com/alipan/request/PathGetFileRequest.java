package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 文件路径查找文件
 */
@Data
@Builder
public class PathGetFileRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 	必填
     * 	file_path
     */
    @JSONField(name = "file_path")
    private String file_path;

}






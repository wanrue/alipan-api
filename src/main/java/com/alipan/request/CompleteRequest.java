package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

/**
 * 标记文件上传完毕
 */
@Data
public class CompleteRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 必填
     * 文件创建获取的upload_id
     */
    @JSONField(name = "upload_id")
    private String uploadId;

}
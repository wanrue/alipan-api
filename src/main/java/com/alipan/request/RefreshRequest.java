package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 刷新获取上传地址
 */
@Data
@Builder
public class RefreshRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 必填
     * upload_id
     */
    @JSONField(name = "upload_id")
    private String uploadId;


    /**
     * 分片信息列表
     */
    @JSONField(name = "part_info_list")
    private List<PartInfo> partInfoList;

    @Data
    @AllArgsConstructor
    public static class PartInfo {
        /**
         * 分片编号
         */
        @JSONField(name = "part_number")
        private Integer partNumber;
    }
}






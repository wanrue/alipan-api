package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 复制文件或文件夹
 */
@Data
@Builder
public class CopyRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 必填
     * 父文件ID、根目录为 root
     */
    @JSONField(name = "to_parent_file_id")
    private String toParentFileId;

    /**
     * 当目标文件夹下存在同名文件时，是否自动重命名，默认为 false，默认允许同名文件
     */
    @JSONField(name = "auto_rename")
    private boolean autoRename;

}
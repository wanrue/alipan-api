package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 获取文件详情
 */
@Data
@Builder
public class GetFileRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 	必填
     * 	file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 选填
     * 生成的视频缩略图截帧时间，单位ms，默认120000ms
     */
    @JSONField(name = "video_thumbnail_time")
    private Integer videoThumbnailTime;

    /**
     * 选填
     * 生成的视频缩略图宽度，默认480px
     */
    @JSONField(name = "video_thumbnail_width")
    private Integer videoThumbnailWidth;

    /**
     * 选填
     * 生成的图片缩略图宽度，默认480px
     */
    @JSONField(name = "image_thumbnail_width")
    private Integer imageThumbnailWidth;

}






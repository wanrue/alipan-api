package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 批量获取文件详情
 */
@Data
@Builder
public class BathGetRequest {

    /**
     * 必填
     * file_list
     */
    @JSONField(name = "file_list")
    private List<BatchRequest> fileList;

    @Data
    public static class BatchRequest {
        /**
         * 必填
         * drive id
         */
        @JSONField(name = "drive_id")
        private String driveId;

        /**
         * 必填
         * file_id
         */
        @JSONField(name = "file_id")
        private String fileId;
    }

}






package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 列举已上传分片
 */
@Data
@Builder
public class ListPartRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 必填
     * 文件创建获取的upload_id
     */
    @JSONField(name = "upload_id")
    private String uploadId;

    /**
     *
     */
    @JSONField(name = "part_number_marker")
    private String partNumberMarker;

}






package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 文件创建
 */
@Data
@Builder
public class CreateRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 必填
     * 父文件ID、根目录为 root
     */
    @JSONField(name = "parent_file_id")
    private String parentFileId;

    /**
     * 必填
     * 文件名称，按照 utf8 编码最长 1024 字节，不能以 / 结尾
     */
    private String name;

    /**
     * 必填
     * file | folder
     */
    private String type;

    /**
     * 必填
     * auto_rename 自动重命名，存在并发问题
     * refuse 同名不创建
     * ignore 同名文件可创建
     */
    @JSONField(name = "check_name_mode")
    private String checkNameMode;


    /**
     * 分片信息列表
     * 选填
     * 最大分片数量 10000
     */
    @JSONField(name = "part_info_list")
    private List<PartInfo> partInfoList;

    /**
     * 选填
     * 仅上传livp格式的时候需要，常见场景不需要
     */
    @JSONField(name = "streams_info")
    private List<Stream> streams_info;

    @Data
    public static class PartInfo {
        /**
         * 分片编号
         */
        @JSONField(name = "part_number")
        private Integer partNumber;

    }

    @Data
    public static class Stream {
        @JSONField(name = "content_hash")
        private String contentHash;
        @JSONField(name = "content_hash_name")
        private String contentHashName;
        @JSONField(name = "proof_version")
        private String proofVersion;
        @JSONField(name = "proof_code")
        private String proofCode;
        @JSONField(name = "content_md5")
        private String contentMd5;
        @JSONField(name = "pre_hash")
        private String preHash;
        @JSONField(name = "size")
        private String size;
        @JSONField(name = "part_info_list")
        private List<PartInfo> partInfoList;
    }

    /**
     * 选填
     * 针对大文件sha1计算非常耗时的情况， 可以先在读取文件的前1k的sha1， 如果前1k的sha1没有匹配的， 那么说明文件无法做秒传， 如果1ksha1有匹配再计算文件sha1进行秒传，这样有效边避免无效的sha1计算。
     */
    @JSONField(name = "pre_hash")
    private String preHash;
    /**
     * 选填
     * 秒传必须 文件大小，单位为 byte
     */
    private Long size;

    /**
     * 选填
     * 文件内容 hash 值，需要根据 content_hash_name 指定的算法计算，当前都是sha1算法
     */
    @JSONField(name = "content_hash")
    private String contentHash;
    /**
     * 选填
     * 秒传必须
     */
    @JSONField(name = "content_hash_name")
    private String contentHashName;
    /**
     * 选填
     * 秒传必须
     */
    @JSONField(name = "proof_code")
    private String proofCode;
    /**
     * 选填
     * 固定 v1
     */
    @JSONField(name = "proof_version")
    private String proofVersion;
    /**
     * 选填
     * 本地创建时间，格式yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
     */
    @JSONField(name = "local_created_at")
    private String localCreatedAt;
    /**
     * 选填
     * 本地修改时间，格式yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
     */
    @JSONField(name = "local_modified_at")
    private String localModifiedAt;

}






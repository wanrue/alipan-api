package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 删除文件
 */
@Data
@Builder
public class DeleteRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;
    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

}
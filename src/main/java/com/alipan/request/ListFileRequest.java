package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 获取文件列表
 */
@Data
@Builder
public class ListFileRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 选填
     * 返回文件数量   默认 50，最大 100
     */
    private Integer limit;

    /**
     * 选填
     * 分页标记
     */
    private String marker;

    /**
     * 选填
     * created_at updated_at name size
     */
    @JSONField(name = "order_by")
    private String orderBy;

    /**
     * 选填
     * DESC ASC
     */
    @JSONField(name = "order_direction")
    private String orderDirection;

    /**
     * 必填
     * 根目录为root
     */
    @JSONField(name = "parent_file_id")
    private String parentFileId;

    /**
     * 选填
     * all | file | folder， 默认所有类型 type为folder时，category不做检查
     */
    private String type;

    /**
     * 选填
     * 生成的视频缩略图截帧时间，单位ms，默认120000ms
     */
    @JSONField(name = "video_thumbnail_time")
    private Integer videoThumbnailTime;

    /**
     * 选填
     * 生成的视频缩略图宽度，默认480px
     */
    @JSONField(name = "video_thumbnail_width")
    private Integer videoThumbnailWidth;


    /**
     * 选填
     * 生成的图片缩略图宽度，默认480px
     */
    @JSONField(name = "image_thumbnail_width")
    private Integer imageThumbnailWidth;

    /**
     * 选填	当填 * 时，返回文件所有字段；
     * 1.当只需要特定字段时，可设置为：url、thumbnail、video_metadata，
     * 2.当需要多个字段时，以逗号 , 分割，如 url,thumbnail
     */
    private String fields;

}






package com.alipan.request;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Builder;
import lombok.Data;

/**
 * 获取文件下载链接
 * https://www.yuque.com/aliyundrive/zpfszx/gogo34oi2gy98w5d#mN50J
 */
@Data
@Builder
public class DownloadRequest {

    /**
     * 必填
     * drive id
     */
    @JSONField(name = "drive_id")
    private String driveId;

    /**
     * 必填
     * file_id
     */
    @JSONField(name = "file_id")
    private String fileId;

    /**
     * 下载地址过期时间，单位为秒，最长 32小时 115200 秒，默认为 900 秒
     */
    @JSONField(name = "expire_sec")
    private Integer expireSec = 115200;

}






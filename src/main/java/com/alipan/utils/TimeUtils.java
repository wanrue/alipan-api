package com.alipan.utils;

import cn.hutool.http.HttpUtil;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

public class TimeUtils {
    public static boolean expires(String fileUrl) {
        Map<String, String> paramsMap = HttpUtil.decodeParamMap(fileUrl, Charset.defaultCharset());
        String expires = paramsMap.get("x-oss-expires");
        if (expires == null || new Date().getTime() / 1000 > Long.parseLong(expires) - 100) {
            return true;
        }
        return false;
    }
}

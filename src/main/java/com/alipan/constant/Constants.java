package com.alipan.constant;

public class Constants {

    /**
     * URL
     */
    public static String URL = "https://openapi.aliyundrive.com";

    /**
     * 通过 code 获取 access_token 或通过 refresh_token 刷新 access_token。code 10分钟内有效，只能用一次。
     */
    public static String ACCESS_TOKEN = "/oauth/access_token";

    /**
     * 通过 access_token 获取用户信息
     */
    public static String USERS_INFO = "/oauth/users/info";

    /**
     * 用户信息-获取用户空间信息
     */
    public static String SPACE_INFO = "/adrive/v1.0/user/getSpaceInfo";

    /**
     * 用户信息-获取用户drive信息
     */
    public static String DRIVE_INFO = "/adrive/v1.0/user/getDriveInfo";

    /**
     * 文件列表-获取文件列表
     */
    public static String OPEN_FILE_LIST = "/adrive/v1.0/openFile/list";

    /**
     * 文件列表-文件搜索
     */
    public static String OPEN_FILE_SEARCH = "/adrive/v1.0/openFile/search";

    /**
     * 文件列表-获取收藏文件列表
     */
    public static String OPEN_FILE_STARRED_LIST = "/adrive/v1.0/openFile/starredList";

    /**
     * 获取文件详情-获取文件详情
     */
    public static String OPEN_FILE_GET = "/adrive/v1.0/openFile/get";

    /**
     * 获取文件详情-批量获取文件详情
     */
    public static String OPEN_FILE_BATCH_GET = "/adrive/v1.0/openFile/batch/get";

    /**
     * 获取文件详情-根据文件路径查找文件
     */
    public static String OPEN_FILE_GET_BY_PATH = "/adrive/v1.0/openFile/get_by_path";

    /**
     * 获取文件详情-获取文件下载详情
     */
    public static String OPEN_FILE_GET_DOWNLOAD_URL = "/adrive/v1.0/openFile/getDownloadUrl";

    /**
     * 文件上传-创建文件
     */
    public static String OPEN_FILE_CREATE = "/adrive/v1.0/openFile/create";

    /**
     * 文件上传-刷新获取上传地址
     */
    public static String OPEN_FILE_UPLOAD_URL = "/adrive/v1.0/openFile/getUploadUrl";

    /**
     * 文件上传-列举已上传分片
     */
    public static String OPEN_FILE_LIST_PART = " /adrive/v1.0/openFile/listUploadedParts";

    /**
     * 文件上传-标记文件上传完毕
     */
    public static String OPEN_FILE_COMPLETE = "/adrive/v1.0/openFile/complete";

    /**
     * 文件更新-重命名单个文件，收藏单个文件
     */
    public static String OPEN_FILE_UPDATE = "/adrive/v1.0/openFile/update";

    /**
     * 移动文件或文件夹
     */
    public static String OPEN_FILE_MOVE = "/adrive/v1.0/openFile/move";

    /**
     * 复制文件或文件夹
     */
    public static String OPEN_FILE_COPY = "/adrive/v1.0/openFile/copy";

    /**
     * 放入回收站
     */
    public static String OPEN_FILE_TRASH = "/adrive/v1.0/openFile/recyclebin/trash";

    /**
     * 删除文件
     */
    public static String OPEN_FILE_DELETE = "/adrive/v1.0/openFile/delete";
}

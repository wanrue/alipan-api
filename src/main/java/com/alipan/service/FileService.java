package com.alipan.service;

import com.alipan.request.*;
import com.alipan.response.*;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface FileService {

    /**
     * 获取文件列表-获取文件列表
     *
     * @param request
     * @return
     */
    ListFileResponse fileList(Map<String, String> headers, ListFileRequest request);

    /**
     * 获取文件列表-文件搜索
     *
     * @param request
     * @return
     */
    SearchResponse searchFile(Map<String, String> headers, SearchRequest request);

    /**
     * 获取文件列表-获取收藏文件列表
     *
     * @param request
     * @return
     */
    StarredResponse starredFileList(Map<String, String> headers, StarredRequest request);

    /**
     * 获取文件详情-获取文件详情
     *
     * @param request
     * @return
     */
    GetFileResponse fileInfo(Map<String, String> headers, GetFileRequest request);

    /**
     * 获取文件详情-文件路径查找文件
     *
     * @param request
     * @return
     */
    List<GetFileResponse> searchFileByPath(Map<String, String> headers, PathGetFileRequest request);

    /**
     * 获取文件详情-批量获取文件详情
     *
     * @param request
     * @return
     */
    List<GetFileResponse> bathFileInfo(Map<String, String> headers, BathGetRequest request);

    /**
     * 获取文件详情-获取文件下载链接
     *
     * @param request
     * @return
     */
    DownloadResponse downloadFile(Map<String, String> headers, DownloadRequest request);

    /**
     * 文件上传-文件创建
     *
     * @param request
     * @return
     */
    CreateResponse createFile(Map<String, String> headers, CreateRequest request);

    /**
     * 文件上传-刷新获取上传地址
     *
     * @param request
     * @return
     */
    RefreshResponse refreshUploadUrl(Map<String, String> headers, RefreshRequest request);

    /**
     * 文件上传-列举已上传分片
     *
     * @param request
     * @return
     */
    ListPartResponse listPart(Map<String, String> headers, ListPartRequest request);

    /**
     * 文件上传-标记文件上传完毕
     *
     * @param request
     * @return
     */
    CompleteResponse completeFile(Map<String, String> headers, CompleteRequest request);

    /**
     * 文件上传-文件创建
     *
     * @param createResponse
     * @return
     */
    CompleteResponse uploadFile(Map<String, String> headers, CreateResponse createResponse, InputStream inputStream, long length);

    InputStream download(Map<String, String> header, String url, Consumer<Map> consumer);

    /**
     * 文件更新-重命名单个文件，收藏单个文件
     *
     * @param request
     * @return
     */
    RenameResponse renameFile(Map<String, String> headers, RenameRequest request);

    /**
     * 移动或复制文件夹-移动文件或文件夹
     *
     * @param request
     * @return
     */
    MoveResponse moveFile(Map<String, String> headers, MoveRequest request);

    /**
     * 移动或复制文件夹-复制文件或文件夹
     *
     * @param request
     * @return
     */
    CopyResponse copyFile(Map<String, String> headers, CopyRequest request);

    /**
     * 回收站和删除-放入回收站
     *
     * @param request
     * @return
     */
    TrashResponse trashFile(Map<String, String> headers, TrashRequest request);

    /**
     * 回收站和删除-删除文件
     *
     * @param request
     * @return
     */
    DeleteResponse deleteFile(Map<String, String> headers, DeleteRequest request);

}
package com.alipan.service;

import com.alipan.response.DriveInfoResponse;
import com.alipan.response.TokenResponse;

import java.util.Map;

public interface UserService {

    TokenResponse accessToken(String clientId, String secret, String grantType, String code);

    String spaceInfo(Map<String,String> headers);

    DriveInfoResponse driveInfo(Map<String,String> headers);

    String userInfo(Map<String,String> headers);

}
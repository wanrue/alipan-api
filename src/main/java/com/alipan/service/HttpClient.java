package com.alipan.service;

import java.io.InputStream;
import java.util.Map;
import java.util.function.Consumer;

public interface HttpClient {

    String get(Map<String, String> header, String url);

    String post(Map<String, String> header, String url, Object body);

    void upload(Map<String, String> header, String url, byte[] bytes, long size);

    InputStream download(Map<String, String> header, String url, Consumer<Map> consumer);
}

package com.alipan;

import com.alipan.request.CreateRequest;
import com.alipan.request.GetFileRequest;
import com.alipan.request.ListFileRequest;
import com.alipan.request.SearchRequest;
import com.alipan.response.*;
import com.alipan.service.FileService;
import com.alipan.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootTest
public class AliApplicationTests {

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;

    @Test
    public void getDriveInfo() {
        DriveInfoResponse driveInfo = userService.driveInfo(null);
        System.out.printf("");
    }

    @Test
    public void userInfo() {
        String userInfo = userService.userInfo(null);
        System.out.printf("");
    }

    @Test
    public void httpClient() throws Exception {
        String uu = "https://cn-beijing-data.aliyundrive.net/RZWItVUH%2F2260598%2F614826463e5453605ef74d3aa46ee4902529f571%2F614826462baea6bed06542ffaa61ea1a0a63f8a3?di=bj29&dr=2260598&f=614826463e5453605ef74d3aa46ee4902529f571&response-content-disposition=attachment%3B%20filename%2A%3DUTF-8%27%27%25E5%259C%25A8%25E5%25AE%25B6%25E7%258B%2599%25E5%2587%25BB%25E7%2582%25B9%25E4%25BD%258D.png&security-token=CAIS%2BgF1q6Ft5B2yfSjIr5bRefDX2I57%2BaSlelfnjEc%2BON14roP5rDz2IHFPeHJrBeAYt%2FoxmW1X5vwSlq5rR4QAXlDfNVCSeDOxqFHPWZHInuDox55m4cTXNAr%2BIhr%2F29CoEIedZdjBe%2FCrRknZnytou9XTfimjWFrXWv%2Fgy%2BQQDLItUxK%2FcCBNCfpPOwJms7V6D3bKMuu3OROY6Qi5TmgQ41Uh1jgjtPzkkpfFtkGF1GeXkLFF%2B97DRbG%2FdNRpMZtFVNO44fd7bKKp0lQLukMWr%2Fwq3PIdp2ma447NWQlLnzyCMvvJ9OVDFyN0aKEnH7J%2Bq%2FzxhTPrMnpkSlacGoABVeE7epowT82%2BZ2nID5b9dZOBvVDckY3H4hY6E%2BtozNStNfTW4QBWqZoMHzcoojRzcDrAdbIpupaspAsRqQ%2FovYiXGT2HD2WCO99O9%2B2%2FoitHJVJLOFzda2E%2BO2XIytmXZVX6iOgXqurPaUt2hfyj1G0AZdclQ14wM9HUkXBnelY%3D&u=1c94ec25418f44dfaf3a9eec3f114f42&x-oss-access-key-id=STS.NUd2Jc5QZNfNxqVhGk4RTAFRN&x-oss-expires=1687433931&x-oss-signature=LvEF9Z9X8spLxpGTOK84bCCNO%2FGAfcz3GbZ0iAn2mE8%3D&x-oss-signature-version=OSS2";
        URL url = new URL(uu);
        URLConnection conn = url.openConnection();
        InputStream inStream = conn.getInputStream();
        IOUtils.copy(inStream, new FileOutputStream("D:\\data\\" + System.currentTimeMillis() + ".png"));
        System.out.printf("");
    }

    @Test
    public void openFileList() {
        Map map = new HashMap();
        //map.put("x-share-token","eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21Kc29uIjoie1wiZG9tYWluX2lkXCI6XCJiajI5XCIsXCJzaGFyZV9pZFwiOlwiVlB1V3VrQXJuR2lcIixcImNyZWF0b3JcIjpcIjkxYjc2YjcwMmU2MDQ4ZWM5ODY5ODIyZTRkZGYxNDc0XCIsXCJ1c2VyX2lkXCI6XCJhbm9ueW1vdXNcIn0iLCJjdXN0b21UeXBlIjoic2hhcmVfbGluayIsImV4cCI6MTY5Nzk4ODg4MCwiaWF0IjoxNjk3OTgxNjIwfQ.IuGiQqAIcOBhihNGsIDQ4ViHNy1IS3MCWJcXtLlrGcIuu_P5AkwcJqXz0NHHkFj9sxA-ENZSwfNnX98RIlVmXpQKjybWKe7e76y6EWqa_4rBID9NuDAypy2Ibb98u0zNxwl3JwGdLb-wwiAnIAG-8P4r99_IzRCT6hSjsY8FxJo");
        ListFileRequest fileRequest = ListFileRequest.builder()
                .driveId("798084972")
                .parentFileId("64d9213c0c71f8e23aba4ff49f14b63abb4fc04b")
                .build();
        ListFileResponse response = fileService.fileList(map, fileRequest);
        System.out.printf("");
    }

    @Test
    public void openFileSearch() {
        SearchRequest request = SearchRequest.builder().driveId("2260598").parentFileId("root").query("name match \"三\"").build();
        SearchResponse searchResponse = fileService.searchFile(null, request);
        System.out.printf("");
    }

    @Test
    public void uploadFile() throws FileNotFoundException {
        File file = new File("F:\\系统\\ubuntu-22.04.2-live-server-amd64.iso");
        FileInputStream inputStream = new FileInputStream(file);
        CreateRequest request = CreateRequest.builder().driveId("2260598").parentFileId("root").name("ubuntu-22.04.2-live-server-amd64.iso").type("file").checkNameMode("auto_rename").size(file.length()).build();
        CreateResponse createResponse = fileService.createFile(null, request);
        CompleteResponse completeResponse = fileService.uploadFile(null, createResponse, inputStream, file.length());
        System.out.printf("");
    }

    @Test
    public void fileInfo() {
        GetFileRequest request = GetFileRequest.builder()
                .driveId("798084972")
                .fileId("64d9213dd130fce5a8da48678c8fdb944fde99ea")
                .build();
        GetFileResponse fileResponse = fileService.fileInfo(null, request);
        System.out.printf("");
    }


}